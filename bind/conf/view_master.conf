// ========================== Primary =========================================
// DNS presented to internal networks
view "sap" {
    match-clients { any; };
    
    // Allow recursion but only for trusted clients
    recursion yes;
    allow-recursion { trusted; };

    // Forward queries to AD before going out
    forward first;
    forwarders {
        10.255.0.1;
        10.255.128.1;
    };
    
    allow-transfer { xfer; };

    // Only the primary does notifications
    notify no;

    // Needed for recursive queries 
    zone "." IN {
        type hint;
        file "named.root";
    };

    /* Note that all servers listed in NS records
     * are notified by default
     */
    include "/etc/bind/zones_master.conf";
    
    include "/etc/bind/zones_local.conf";
};
