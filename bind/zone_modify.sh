#!/bin/bash

DIR="."
SERIAL=2017060600

PREVIOUS_LINE='NS\sdom2.nix.ardencompanies.com.'
NEW_LINE='\t\tIN\tNS\tdom3.nix.ardencompanies.com.'

for FILE in $(ls .)
do
    grep "" "${FILE}" > /dev/null
    STATUS=$?
    if [ "${STATUS}" -eq 0 ]
    then
        sed -i -r -e 's/(.*\s)[0-9]{10}(.*)/\1'"${SERIAL}"'\2/g' \
            -e '/'${PREVIOUS_LINE}'/ a\'${NEW_LINE} "${FILE}"
    fi
done
