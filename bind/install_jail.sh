#!/bin/sh

# Based on http://www.tldp.org/HOWTO/Chroot-BIND-HOWTO-2.html

# Path Constants 
JAIL_ROOT="/var/bind-jail/"
STD_ROOT="/"
SRC_ROOT="/tmp/bind-conf/"
SRC_CONF="${SRC_ROOT}/conf"
SRC_PRI="${SRC_ROOT}/pri"

# Constant variables
LOG_SYSLOG="syslog"
LOG_NAMED="named"
ROLE_PRIMARY="primary"
ROLE_MASTER="master"
ROLE_SLAVE="slave"
DEPLOYMENT_JAIL="jail"
DEPLOYMENT_ROOT="root"

# Root hints file
ROOT_HINT_URL="https://www.internic.net/domain/named.root"

# Arguments
ROLE=$1

#==== Set Paths ===============================================================
# Configure the default variables based on the provided root directory
set_paths() {
    BIND_ROOT="${1}"
    BIND_PRI="${BIND_ROOT}/var/bind/pri"
    BIND_SEC="${BIND_ROOT}/var/bind/sec"
    BIND_DYN="${BIND_ROOT}/var/bind/dyn"
    BIND_VAR_BIND="${BIND_ROOT}/var/bind"
    BIND_CONF="${BIND_ROOT}/etc/bind"
    BIND_RUN="${BIND_ROOT}/var/run/named"
#   BIND_DEV="${BIND_ROOT}/dev" # Don't remember what this was for
    SRC_ROOT="/tmp/bind-conf/"
    SRC_CONF="${SRC_ROOT}/conf"
    SRC_PRI="${SRC_ROOT}/pri"
}

#==== Deployment ==============================================================
# Create the jail directories & populate them with our configuration files
# or create the local filesystems and deploy the files as they should exist
prep_deployment() {
    DEPLOYMENT_MODE="${1}"
    case "${DEPLOYMENT_MODE}" in
        "${DEPLOYMENT_ROOT}")
            set_paths "${STD_ROOT}"
            if [ -d "${BIND_CONF}" ]
            then
                find "${BIND_CONF}" -type f -exec rm {} \;
                find "${BIND_CONF}" -type l -exec rm {} \;
            fi
            
            if [ -d "${BIND_PRI}" ]
            then
                find "${BIND_PRI}" -type f -exec rm {} \;
            fi

            if [ -d "${BIND_SEC}" ]
            then
                find "${BIND_SEC}" -type f -exec rm {} \;
            fi

            if [ -d "${BIND_DYN}" ]
            then
                find "${BIND_DYN}" -type f -exec rm {} \;
            fi

            if [ -d "${BIND_RUN}" ]
            then
                find "${BIND_RUN}" -type f -exec rm {} \;
            fi
            ;;
        "${DEPLOYMENT_JAIL}")
            # Remove jail directory
            set_paths "${JAIL_ROOT}"

            if [ -d "${BIND_ROOT}" ] && [ "${BIND_ROOT}" != "${STD_ROOT}" ]
            then
                rm -r "${BIND_ROOT}"
            fi
            
            # Set ownership on the root of the jail
            chown "root:${ROOTG}" "${BIND_ROOT}"
            chmod 755 "${BIND_ROOT}"
            ;;
        *)
            echo "Invalid deployment mode: \"${DEPLOYMENT_MODE}\"" 1>&2
            exit 128
            ;;
    esac

    # Create Jail Structure
    mkdir -p "${BIND_CONF}"
    mkdir -p "${BIND_PRI}"
    mkdir -p "${BIND_SEC}"
    mkdir -p "${BIND_DYN}"
    mkdir -p "${BIND_RUN}"

    # Install the configuration files
    CONF_FILES=$(find "${SRC_CONF}" -type f)
    for FILE in ${CONF_FILES}
    do
        chown "root:${ROOTG}" "${FILE}"
        chmod 644 "${FILE}"
        cp "${FILE}" "${BIND_CONF}/"
    done

    # Install the primary zone files
    PRI_FILES=$(find "${SRC_PRI}" -type f)
    for FILE in ${PRI_FILES}
    do
        chown "root:${ROOTG}" "${FILE}"
        chmod 644 "${FILE}"
        cp "${FILE}" "${BIND_PRI}/"
    done

    # Install the root hint cache
    if [ -e "${BIND_VAR_BIND}/named.root" ]
    then
        rm "${BIND_VAR_BIND}/named.root"
    fi
    wget -P "${BIND_VAR_BIND}" "${ROOT_HINT_URL}"
    chmod 640 "${BIND_VAR_BIND}/named.root"

    # Set ownership on the directory structure
    chown -R root:named "${BIND_CONF}"
    chown -R root:named "${BIND_VAR_BIND}"
    chown -R named:named "${BIND_DYN}"
    chown -R named:named "${BIND_SEC}"
    chown -R root:named "${BIND_RUN}"

    # Correct the directory structure permissions
    chmod 750 "${BIND_CONF}"
    find "${BIND_CONF}" -type f -exec chmod 640 {} \; 
    chmod -R 770 "${BIND_VAR_BIND}"
    chmod 750 "${BIND_PRI}"
    find "${BIND_PRI}" -type f -exec chmod 640 {} \; 
    chmod 770 "${BIND_RUN}"
}

#==== RNDC Configuration ======================================================
# Generate the key files for the RNDC utility. Also place them appropriately
prep_rndc() {
    RNDC_CONF_FILE=/etc/rndc.conf
    RNDC_KEY_FILE="${BIND_CONF}/rndc.key"

    # Configure RNDC
    rndc-confgen -b 512 -r /dev/urandom | grep -v "^$" | \
        grep -v "^#" > "${RNDC_CONF_FILE}"
    chmod 640 "${RNDC_CONF_FILE}"
    chown "root:${ROOTG}" "${RNDC_CONF_FILE}"

    # Get the RNDC key in place in our jail
    head -n 4 "${RNDC_CONF_FILE}" > "${RNDC_KEY_FILE}"
    chmod 640 "${RNDC_KEY_FILE}"
    chown root:named "${RNDC_KEY_FILE}"
}

prep_logging() {

    LOG_MODE=$1
    
    # Get to the config directory
    cd "${BIND_CONF}/" || exit

    case "${LOG_MODE}" in
        # Configure Syslog based logging
        "${LOG_SYSLOG}")
            ln -s log_syslog.conf log.conf
            ;;
        # Configure standard named controlled logging
        "${LOG_NAMED}")
            ln -s log_named.conf log.conf
            ;;
        *)
            echo "Invalid log mode: \"${LOG_MODE}\"" 1>&2
            exit 128
    esac
    
    # Return to where we started
    cd - || exit
}

#==== Linux Specific Configuration ============================================
# Linux installs of bind actually support some features we don't have on AIX. 
# This is a super coarse way of checking but whatever.
prep_linux() {
    # Link the 9.10 compatabile named.conf
    cd "${BIND_CONF}" || exit
    ln -s "named_9.10.conf" "named.conf"
    cd - || exit
}

#==== AIX Specific Configuration ==============================================
# AIX bind installs have a slightly different structure
prep_aix() {
    # Link the 9.4 compatabile named.conf
    cd "${BIND_CONF}" || exit
    ln -s "named_9.4.conf" "named.conf"
    cd - || exit

    # Update the AIX subsystem definition for named to include the cli arguments
    # We have to tell it where the conf file is because default is
    # /etc/named.conf
    chssys -s named -u named -a "-c ${BIND_CONF}/named.conf"
}

#==== Role Assignment =========================================================
# Assign a view configuration based on the provided dataset
prep_role() {

    LOCAL_ROLE=$1

    cd "${BIND_ROOT}/etc/bind/" || exit

    case "${LOCAL_ROLE}" in
        "${ROLE_SLAVE}")
            ln -s "view_${ROLE_SLAVE}.conf" "view.conf"
            ;;
        "${ROLE_MASTER}")
            ln -s "view_${ROLE_MASTER}.conf" "view.conf"
            ;;
        "${ROLE_PRIMARY}")
            ln -s "view_${ROLE_PRIMARY}.conf" "view.conf"
            ;;
        *)
            echo "Invalid role \"${LOCAL_ROLE}\"!" 1>&2
            exit 128
            ;;
    esac

    cd - || exit
}

#==== Main ====================================================================
# Determine OS & do OS specific prep
OS_NAME=$(uname)
if [ "${OS_NAME}" = "Linux" ]
then
    ROOTG="root"
    # Configure the jail directory and whatnot
    prep_deployment "${DEPLOYMENT_JAIL}"

    # Generate the RNDC files
    prep_rndc

    # Later conf
    prep_linux
    prep_logging "${LOG_SYSLOG}"
elif [ "${OS_NAME}" = "AIX" ]
then
    ROOTG="system"
    
    # Stop named
    stopsrc -s named

    # AIX src implementation of bind doesn't handle the jail well yet.
    # Switching to unjailed verison.
    prep_deployment "${DEPLOYMENT_ROOT}"

    # Generate the RNDC files
    prep_rndc

    prep_aix
    prep_logging "${LOG_NAMED}"

    # Start named
    startsrc -s named
fi

# Configure the role of this server
if [ -z "${ROLE}" ]
then
    ROLE="${ROLE_SLAVE}"
fi
prep_role "${ROLE}"
