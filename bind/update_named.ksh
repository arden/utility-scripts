#!/usr/bin/ksh93
# Constants
NAME_INDEX=0
TYPE_INDEX=1
TYPE_MASTER="master"
TYPE_SLAVE="slave"
TYPE_MSLAVE="mslave"

# Prefix
FPRE=named.conf

# Paths and files
FPATH=/arden/conf/var/named/etc
LFILE=/arden/scripts/dns/server_list
NHEAD=named.conf.header
NLCL=named.conf.localzones
NMASTER=named.conf.masterzones
NSLAVE=named.conf.slavezones
NMSLAVE=named.conf.mslavezones
TARGET=$FPATH
RPATH=/var/named/etc

# Update the master slave zone file (saves time)
cat ${FPATH}/${NSLAVE} | sed 's/172.17.17.203; //' > ${FPATH}/${NMSLAVE}

# Loop through the listing and build our files
while read LINE
do
  set -A FILE_ARRAY ${LINE}
 
  # Erase the target
  echo "//------------------------ ${FILE_ARRAY[$NAME_INDEX]}" > "${TARGET}/${FPRE}.${FILE_ARRAY[$NAME_INDEX]}"

  # Backup the old file
  scp ${FILE_ARRAY[$NAME_INDEX]}:/${RPATH}/${FPRE}.${FILE_ARRAY[$NAME_INDEX]} ${TARGET}/${FPRE}.${FILE_ARRAY[$NAME_INDEX]}.bak
  # Store the header, rndc, and localzones sections
  cat "${FPATH}/${NHEAD}" >> "${TARGET}/${FPRE}.${FILE_ARRAY[$NAME_INDEX]}"
  cat "${FPATH}/${FPRE}.${FILE_ARRAY[$NAME_INDEX]}.rndc" >> "${TARGET}/${FPRE}.${FILE_ARRAY[$NAME_INDEX]}"
  cat "${FPATH}/${NLCL}" >> "${TARGET}/${FPRE}.${FILE_ARRAY[$NAME_INDEX]}"

  case ${FILE_ARRAY[$TYPE_INDEX]} in
    "${TYPE_MASTER}")
      cat "${FPATH}/${NMASTER}" >> "${TARGET}/${FPRE}.${FILE_ARRAY[$NAME_INDEX]}"
      ;;
    "${TYPE_SLAVE}")
      cat "${FPATH}/${NSLAVE}" >> "${TARGET}/${FPRE}.${FILE_ARRAY[$NAME_INDEX]}"
      ;;
    "${TYPE_MSLAVE}")
      cat "${FPATH}/${NMSLAVE}" >> "${TARGET}/${FPRE}.${FILE_ARRAY[$NAME_INDEX]}"
      ;;
  esac

  # Send the new file back
  scp ${TARGET}/${FPRE}.${FILE_ARRAY[$NAME_INDEX]} ${FILE_ARRAY[$NAME_INDEX]}:/${RPATH}/${FPRE}.${FILE_ARRAY[$NAME_INDEX]}
done <"$LFILE"
