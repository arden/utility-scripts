#!/usr/bin/sh
ME=$(hostname)
FPATH=/arden/conf/named.conf

echo "target:\t${FPATH}.${ME}"
cp /etc/named.conf ${FPATH}.${ME}
