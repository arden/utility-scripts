#!/bin/bash

for SOURCE in $(find  . -type f -name "master*")
do
    TARGET=$(echo "${SOURCE}" | sed -r \
        's/[.][/]master[.]([a-zA-Z0-9.]+)/\1.zone/')
    echo "${SOURCE} ./${TARGET}"
    mv "${SOURCE}" "./${TARGET}"
done

for SOURCE in $(find  . -type f -name "*rev")
do
    TARGET=$(echo "${SOURCE}" | sed -r \
        's/[.][/]([a-zA-Z0-9.]+)[.]rev/rev.\1.zone/')
    echo "${SOURCE} ./${TARGET}"
    mv "${SOURCE}" "./${TARGET}"
done

