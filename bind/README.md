Note that all components of this process must be run from within the AIX 
environment. Specifically, nim.nix.ardencompanies.com is the primary server!

# Creating new Zones

The following three files may need to be modified to deploy this change:

| Path   | Description   |
| ------ | ------------- |
| /arden/conf/var/named/etc/named.conf.masterzones | Used by the actual master server (dom1.nix) |
| /arden/conf/var/named/etc/named.conf.mslavezones | Utilized by the slave at the master level (dom2.nix) |
| /arden/conf/var/named/etc/named.conf.slavezones | All other AIX DNS servers utilize this configuration |

## Zoneset Files

Any new zoneset must be added to all three tiers for proper operation. 
In this example we will be inserting the following zone:

```shell
zone "lp0.sap.ardencompanies.com" in {
};
```

In masterzones the type and allow transfer settings ensure that all slaves are
able to appropriately copy the file sets.

```shell
zone "lp0.sap.ardencompanies.com" in {
    type master;
    file "master/master.lpo.sap.ardencompanies.com";
    allow-transfer{ slaves; };
};
```

One of the two "masters" in our environment is actually a slave as bind with a
files backed does not support true multi-master functionality.

```shell
zone "lp0.sap.ardencompanies.com" in {
        type slave;
        file "slave/slave.lp0.sap.ardencompanies.com";
        allow-transfer{ slaves; };
        allow-notify{ masters; slaves; };
        masters{ 172.17.17.202; };
};
```

Finally, the remaining servers see both the false and true masters as "masters"

```shell
zone "lp0.sap.ardencompanies.com" in {
        type slave;
        file "slave/slave.lp0.sap.ardencompanies.com";
        allow-transfer{ slaves; };
        allow-notify{ masters; slaves; };
        masters{ 172.17.17.202; 172.17.17.203; };
};
```

## Deployment

Once the new zoneset files are updated in `/arden/conf/var/named/etc/` actually deploying
the new configuration is a simple matter of issuing the following commands on nim.nix as root.

```bash
cd /arden/scripts/dns
./update_named.ksh
dsh -v -N aix_dns 'rndc reload' | dshbak
```
