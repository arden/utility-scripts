#!/bin/sh

SOURCE=/arden/conf/bind-jail/
BIND_CONF=/etc/bind/
BIND_VAR=/var/bind
BIND_PRI="${BIND_VAR}/pri"

# Ensure /arden is mounted
mount /arden || exit

stopsrc -s named
## Config update
cd "${BIND_CONF}" || exit
find . -type f -name "*.conf" -exec rm {} \;
find "${SOURCE}" -type f -name "*.conf" -exec cp {} . \;
find . -type f -name "*.conf" -exec chown root:named {} . \;

## Zone update
cd "${BIND_VAR}" || exit
find . -type f -name "*.zone" -exec rm {} \;
cd "${BIND_PRI}" || exit
find "${SOURCE}" -type f -name "*.zone" -exec cp {} . \;
find . -type f -name "*.zone" -exec chown root:named {} . \;

cd / || exit

## Commit Changes
startsrc -s named
lssrc -l -s named
