# VM Specification

Attribute | Value
----------|-------
vCPU | 2
Memory (MiB) | 512
Adapter Type | e1000
Disk (GiB) | 8

# Install

1. Mount the CD Rom and force it to boot from CD.
2. Run `setup-alpine`
3. Choose US keyboard
4. Hostname `nix-dom3`
5. Choose `eth0`
6. IP address `10.160.0.13/24`
7. Gateway `10.0.160.1`
8. Domain `ardencompanies.com`
9. Create password entry in pleasant for local root user
10. EST timezone
11. No proxy for network
12. Openssh server
13. chrony NTP server/client
14. Install on `sda` using `sys`
15. Create DNS entry & DHCP reservation 00505685003e
16. Update apk `apk update`
17. Install open-vm-tools via the following

    ```bash
    apk add open-vm-tools
    rc-service open-vm-tools start
    rc-update add open-vm-tools default
    ```
18. Install sudo

    ```bash
    apk add sudo sudo-doc
    visudo
    ```
    Uncomment the line which gives `wheel` members permission to sudo.
19. Install vim

    ```bash
    apk vim vim-doc
    ```
19. Add a local rescue user for remote access

    ```bash
    GROUPS="wheel"
    adduser -s /bin/ash rescue
    ```

# Entropy

1. Install the Haveged daemon.

    ```bash
    apk add haveged haveged-doc
    ```
2. Check current entropy, start the service, and check again.

    ```bash
    cat /proc/sys/kernel/random/entropy_avail
    rc-service haveged start
    cat /proc/sys/kernel/random/entropy_avail
    ```
3. Add the service to the default run-level.

    ```bash
    rc-update add haveged default
    ```

**We need a better source of randomness**

# Rsyslog

Seems like the log to use.

1. Install the following packages.

    ```bash
    apk add rsyslog rsyslog-doc
    ```
2. Add it to the default run-level and start it. Also, try restarting.

    ```bash
    rc-update add rsyslog default
    ```

**Needs log rotation configuration and/or graylog**
**klogd is still running**

# DNS (BIND)

1. Install the following packages via apk.

    ```bash
    apk add bind bind-doc bind-tools
    ```
2. Add a new entry to the syslog config for DNS logging in `/etc/rsyslog.conf`

    ```bash
    # Save messages related to DNS to named.log
    local0.*                                    /var/log/named.log
    ```
3. Restart the syslog daemon

    ```bash
    rc-service rsyslog restart
    ```
4. Acquire the current version of the config files from [this repo](https://gitlab.com/arden/utility-scripts/tree/master/bind).
5. Store the files from step 4 in `/tmp/bind-conf`. 
6. Prepare the jail.

    ```bash
    cd /tmp/bind-conf/
    bash prep_jail.sh
    ```
7. Configure RNDC

    ```bash
    cd /tmp/bind-conf/
    bash prep_rndc.sh
    ```
8. Complete any linux specific configuration.

    ```bash
    cd /tmp/bind-conf/
    bash prep_linux.sh
    ```
9. Create the appropriate view link to set the server as either **primary**, **master**, or **slave**.\

    ```bash
    cd /tmp/bind-conf/
    bash prep_master.sh
    ```
10. TODO service config

    ```bash
    named -t /var/bind-jail -u named -c /etc/bind/named.conf
    ```

# Chrony / NTP Setup

# Packages 



