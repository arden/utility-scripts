#!/usr/bin/python3
import re
import argparse
import csv
from datetime import datetime
import sys
import os

PORT_STRING_REGEX = '^\[([0-9:]+)\]\s+disk\s+fc:0x([0-9a-f]+)(0x[0-9a-f]+)\s+\/dev\/([a-z]+)\s+\[([0-9])\]\s+(\w+)\s+fc:0x([0-9a-f]+)(0x[0-9a-f]+)$'
PORT_STRING = re.compile(PORT_STRING_REGEX, re.DOTALL)
FILENAME = re.compile(r'lsscsi_([a-z0-9-]+)[.]out')

def _match_fc_properties(port_string) -> dict:
    data = {}
    match = PORT_STRING.match(port_string)
    if match is not None:
        data['scsi_path'] = match[1]
        data['target_port_name'] = match[2]
        data['target_fcid'] = match[3]
        data['disk_name'] = match[4]
        data['host_id'] = match[5]
        data['host_device'] = match[6]
        data['host_port_name'] = match[7]
        data['host_fcid'] = match[8]

    return data

def _extract_servername(file_name) -> str:
    basename = os.path.basename(file_name)
    match = FILENAME.match(basename)
    if match:
        return match[1]
    else:
        raise NameError("Invalid filename!")

def main():
    DESC_TEXT = 'Parses the output of "lsscsi -Ht & lsscsi-t "'
    PARSER = argparse.ArgumentParser(description=DESC_TEXT)

    PARSER.add_argument(
        "-i",
        "--input",
        dest='input',
        required=True,
        help="File to parse"
        )
    args = PARSER.parse_args()

    input_path = args.input
    server_name = _extract_servername(input_path)

    parsed_paths = []
    with open(input_path) as f:
        for line in f:
            parsed_line = _match_fc_properties(line)
            parsed_line['server_name'] = server_name
            parsed_paths.append(parsed_line)

    if len(parsed_paths) > 0:
        keys = parsed_paths[0].keys()
        dict_writer = csv.DictWriter(sys.stdout, keys)
        dict_writer.writerows(parsed_paths)

if __name__ == "__main__":
    main()
