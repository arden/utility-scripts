#!/usr/bin/python3
import re
import argparse
import csv
from datetime import datetime
import sys
import os

PORT_STRING_REGEX = '.*\s+node_name\s+= \"([a-z0-9]+)\".*port_id\s+= \"([a-z0-9]+)\".*port_name\s+= \"([a-z0-9]+)\".*port_state\s+= \"([a-zA-Z]+)\"'
PORT_STRING = re.compile(PORT_STRING_REGEX, re.DOTALL)
END_BLOCK = re.compile(r'\s{6}uevent\s{14}=')
FILENAME = re.compile(r'fc_host_([a-z0-9-]+)[.]out')

def _match_fc_properties(port_string) -> dict:
    data = {}
    match = PORT_STRING.match(port_string)
    if match is not None:
        data['node_name'] = match[1]
        data['port_id'] = match[2]
        data['port_name'] = match[3]
        data['port_state'] = match[4]

    return data

def _extract_servername(file_name) -> str:
    basename = os.path.basename(file_name)
    match = FILENAME.match(basename)
    if match:
        return match[1]
    else:
        raise NameError("Invalid filename!")

def main():
    DESC_TEXT = 'Parses the output of "systool -c fc_remote_ports -v"'
    PARSER = argparse.ArgumentParser(description=DESC_TEXT)

    PARSER.add_argument(
        "-i",
        "--input",
        dest='input',
        required=True,
        help="File to parse"
        )
    args = PARSER.parse_args()

    input_path = args.input
    server_name = _extract_servername(input_path)

    parsed_paths = []
    with open(input_path) as f:
        block = ""
        for line in f:
            if not END_BLOCK.match(line):
                block = block + line
            else:
                parsed_block = _match_fc_properties(block)
                parsed_block['server_name'] = server_name
                parsed_paths.append(parsed_block)
                block = ""

    keys = parsed_paths[0].keys()
    dict_writer = csv.DictWriter(sys.stdout, keys)
    dict_writer.writerows(parsed_paths)

if __name__ == "__main__":
    main()
