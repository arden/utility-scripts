1. Generate the fibre channel path status information on all of the PPC64 hosts.

    ```bash
    bolt command run 'systool -c fc_host -v > "/tmp/fc_host_$(hostname).out"' --targets ppc64_bfm
    ```
1. Retrieve the data generated in the previous step

    ```bash
    LOC=test_data
    mkdir -p "${LOC}"
    cd "${LOC}"
    for n in $(<../node-list.txt); do echo "get /tmp/fc_host*" | sftp ${n}; done
    cd -
    ```

1. Parse the data retrieved in the previous step and generated a CSV

    ```bash
    find "${LOC}" -type f -exec ./parse-status.py -i {} \; > status_$(date +%F_%H%M).csv
    ```
1. Get the fibre channel login data from the switches in the environment.

    ```bash
    SWITCHES=("d0-nsw0221.bf0.arden" "d0-nsw0222.bf0.arden")
    for S in ${SWITCHES}; do
      ssh -o PubkeyAuthentication=no "adm_pdemonaco@${S}" "sh flogi database" > "${S}_$(date +%F_%H%M).out";
    done
    ```
1. Compare the switch login data to the fibre channel IDs from the hosts and identify missing and found

    ```bash
    STATUS_FILE=status_2024-03-11_1639.csv
    SWITCH_FILE=d0-nsw0222.bf0.arden_2024-03-11_1557.out
    ./check_missing.sh "${STATUS_FILE}" "${SWITCH_FILE}"
    ```
