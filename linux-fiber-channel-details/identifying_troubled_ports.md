# Summary

If high latency is observed and we see strange behavior in `dmesg` output as shown below we have a bad path in the environment:

```bash
[148102.460581] sd 1:0:0:0: Cancelling outstanding commands.
[148102.485742] sd 1:0:0:0: Successfully cancelled outstanding commands
[148102.485770] sd 1:0:0:0: Aborting outstanding commands
[148102.486180] sd 1:0:0:0: [sda] tag#0 Command (2A) failed: transaction cancelled (2:6) flags: 0 fcp_rsp: 0, resid=0, scsi_sta
tus: 0
[148102.486496] sd 1:0:0:0: Abort successful
```

Identifying faulty paths can be tricky. There are several possible locations where the failures can occur:

1. On the VIO outbound port
2. The MDS switch port connected to the VIO
3. The MDS switch port connected to the storage
4. The port on the storage system

## Remote endpoint

1. Parse the dmesg logs to identify salty ports

  ```bash
  bolt command run 'dmesg -t | grep "Command (2A) failed: transaction cancelled" | awk "''{print \$2}''" | sort | uniq > "/tmp/dmesg_fcabort_$(hostname).out"' --targets ppc64_bfm
  ```

1. Parse out the details using lsscsi on each node

  ```bash
  bolt command run 'while read -r P; do FCTARGET=$(lsscsi -t "${P}"); FCHOST=$(lsscsi -Ht "${P}"); echo "${FCTARGET} ${FCHOST}"; done< "/tmp/dmesg_fcabort_$(hostname).out" > "/tmp/lsscsi_$(hostname).out"' --targets ppc64_bfm
  ```
1. Retrieve the results of lsscsi

  ```bash
  LOC=lsscsi_data
  mkdir -p "${LOC}"
  cd "${LOC}"
  for n in $(<../node-list.txt); do echo "get /tmp/lsscsi_*" | sftp ${n}; done
  cd -
  ```
1. Build the mapping spreadsheet

  ```bash
  find "${LOC}" -type f -exec ./parse-lsscsi.py -i {} \; > "lsscsi_error_paths_$(date +%F_%H%M).csv"
  ```
