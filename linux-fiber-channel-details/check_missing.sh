#!/bin/bash
STATUS_FILE=$1
SWITCH_FILE=$2
FCIDS=$(awk -v FS=',' '$4 ~ /Online/ {print $2":"$5}' "${STATUS_FILE}")
for F in ${FCIDS};
do
    IFS_TEMP="${IFS}"
    SERVER_PARTS=()
    IFS=":"
    for P in $F;
    do 
        SERVER_PARTS+=($P)
    done
    IFS="${IFS_TEMP}"
    FCID="${SERVER_PARTS[0]}"
    SERVER_NAME="${SERVER_PARTS[1]}"
    grep "${FCID}" "${SWITCH_FILE}" >/dev/null
    if [[ 0 -eq $? ]]; then
        echo "${SERVER_NAME} found"
    else
        echo "${SERVER_NAME} missing"
    fi
done
