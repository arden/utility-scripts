#!/bin/bash

STORWIZE=$1
TMP_DEETS="/tmp/fc_deets_${STORWIZE}"

# Get the data from the storwize in the following form
# <nodeID> <portID> <Virtual> <WWPN> <WWNN>
ssh "${STORWIZE}" lstargetportfc | awk -v OFS='\t' \
    '/^[0-9]/ {print $5, $4, $8, $3, $2 }' > "${TMP_DEETS}"

while read -r LINE
do
    # Convert to an array 
    read -r -a FC_PORT <<< "${LINE}"
    if [ 5 -ne ${#FC_PORT[@]} ];
    then
        echo "error: check the output of lstargetportfc!" 1>&2
        exit 1
    fi

    NODE_ID=${FC_PORT[0]}
    PORT_ID=${FC_PORT[1]}
    VIRTUAL=${FC_PORT[2]}
    WWPN=$(echo "${FC_PORT[3]}" | fold -w2 | paste -sd: -)
    WWNN=$(echo "${FC_PORT[4]}" | fold -w2 | paste -sd: -)
    
    # Build the right name
    if [ "${VIRTUAL}" == "yes" ]
    then
        SUFFIX="n${NODE_ID}-${PORT_ID}v"
    else
        SUFFIX="n${NODE_ID}-${PORT_ID}p"
    fi

    printf "%s\t%s\t%s\n" "${SUFFIX}" "${WWPN}" "${WWNN}"
done<"${TMP_DEETS}"

rm "${TMP_DEETS}"
