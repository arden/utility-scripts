#!/bin/bash
RSYNC=$(command -v rsync)
RETRY_PATTERN='\b[0-9]{1,3}\b'
MAX_RETRIES_DEFAULT=50
ARGUMENTS="-r -P --append"
DRY_RUN=$(false; echo $?)
CAFFINATE_CMD=$(command -v caffeinate)
#++ Usage
USAGE="
Usage: ${0} [-d] [-m MAX_RETRIES] SOURCE TARGET
   or: ${0} [-h]

Replicate the content of the directory located at SOURCE to the corresponding directory located at TARGET using rsync. This script will retry the sync on failure until the specified retry count is exceeded or the script fails.

Arguments
  -m, --max-retries MAX_RETRIES    The maximum number of retry attempts which will be made before this script completes. This defaults to ${MAX_RETRIES_DEFAULT} attempts.

  -d, --dry-run     Add the --dry-run flag to the rsync call.

  -h, --help        Display this help and exit."

#-------- Print Error Message -------------------------------------------------
function echoerr()
{
    printf "%s\n" "$*" >&2;
    exit 1
}

#-------- Parse Arguments -----------------------------------------------------
# Shamelessly stolen from 
# https://medium.com/@Drew_Stokes/bash-argument-parsing-54f3b81a6a8f
# Note that the variables in this function are intentionally globals
# shellcheck disable=SC2120
function parse_arguments()
{
    # Collect parameters
    POSITIONAL_IDX=0
    LENGTH="${#ARGS[@]}"
    I=0
    while [ "${I}" -lt "${LENGTH}" ]
    do
        V="${ARGS[$I]}"
        I_NEXT=$((I+1))
        case "$V" in
            -h|--help)
                # Print usage and quit
                echo "${USAGE}"
                exit 0
                ;;
            -m|--max-retries)
                MAX_RETRIES="${ARGS[$I_NEXT]}"
                I=$I_NEXT
                ;;
            -d|--dry-run)
                DRY_RUN=$(true; echo $?)
                ;;
            -*) # unsupported flags
                echoerr "${0}: Unsupported flag ${1}"
                ;;
            *) # preserve positional arguments
                PARAMS[$POSITIONAL_IDX]=$V
                POSITIONAL_IDX=$((POSITIONAL_IDX+1))
                ;;
        esac
        I=$((I+1))
    done

    # Record positional parameters
    SOURCE="${PARAMS[0]}"
    TARGET="${PARAMS[1]}"

    # Check mandatory parameters
    if [ -z "${SOURCE}" ]; then
        echoerr "${0}: SOURCE is mandatory!"
    elif ! [ -d "${SOURCE}" ]; then
        echoerr "${0}: ${SOURCE} does not exist!"
    fi

    if [ -z "${TARGET}" ]; then
        echoerr "${0}: TARGET is mandatory!"
    elif ! [ -d "${TARGET}" ]; then
        echoerr "${0}: ${TARGET} does not exist!"
    elif ! [ -w "${TARGET}" ]; then
        echoerr "${0}: ${TARGET} is not writable!"
    fi

    if [ -z "${MAX_RETRIES}" ]; then
        MAX_RETRIES=$MAX_RETRIES_DEFAULT
    elif ! [[ "${MAX_RETRIES}" =~ $RETRY_PATTERN ]]; then
        echoerr "${0}: Invalid MAX_RETRIES: '${MAX_RETRIES}'"
    fi
}

# Record the parameters - intentionally multiple words
ARGS=( "$@" )
parse_arguments

# Trap interrupts and exit instead of continuing the loop
trap "echo Exited!; exit;" SIGINT SIGTERM

MAX_RETRIES=50
I=0
RC=-1

if [ "${DRY_RUN}" -eq 0 ]
then
    ARGUMENTS="${ARGUMENTS} --dry-run"
fi

CAFFINATE="1"
if [[ "${OSTYPE}" == "darwin"* ]]
then
    CAFFINATE="0"
    if [[ -z "${CAFFINATE_CMD}" ]]
    then
        echo "Caffinate not found, disabling caffinate logic!"
        CAFFINATE="1"
    fi
fi

while [ $RC -ne 0 ] && [ $I -lt $MAX_RETRIES ]
do
    I=$((I+1))
    
    if [ $CAFFINATE -eq 0 ]
    then
        RSYNC_COMMAND="${CAFFINATE_CMD} -s -w ${RSYNC} ${ARGUMENTS}"
    else
        RSYNC_COMMAND="${RSYNC} ${ARGUMENTS}" 
    fi
    echo "${RSYNC_COMMAND} \"${SOURCE}\" \"${TARGET}\""

    # shellcheck disable=SC2086
    $RSYNC_COMMAND "${SOURCE}" "${TARGET}"
    RC=$?
done

# Did we succeed?
if [ $I -eq $MAX_RETRIES ] && [ $RC -ne 0 ]
then
    echoerr "Failed after ${I} attempts."
else
    echo "Sync completed successfully!"
fi
