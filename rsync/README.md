# rsync-retry.sh Script

Simple utility that wraps the rsync command so that it retries on failure.

## Arguments

| Flag | Description |
|----------------|--------------------------------------------------------------------------|
|`-d, --dry-run` | When specified the script perform a test copy instead of actually syncing. |
| `-m, --max-retries` | Override the default number of retry attempts. |


## Setup

1. Download a copy of [rsync-retry.sh](./rsync-retry.sh).
1. Modify it to be executable via `chmod 755 ./rsync-retry.sh`. (From a terminal window in the same folder as the script)

## Example Usage

To execute this script perform the following:

1. Open a bash shell.
1. Execute the script with your desired arguments

    ```bash
    ./rsync-retry.sh "/sample/folder" "/target/folder"
    ```
    ![An example](img/example-execution.png)
1. After this point the script will continue to execute until the sync completes without error or the max retry attempts are exceeded. Successful executions will end with the message `Sync completed successfully!`.

