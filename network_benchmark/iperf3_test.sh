#!/bin/bash

IPERF=$(which iperf3)

# Defaults
TIME=500
PORT=5201
BANDWIDTH="10M"
TIMESTAMP=$(date -u --rfc-3339=seconds)
DIR_RESULT="result"
DIR_WORK="${DIR_RESULT}/iperf3-work"
STAT_FILE="${DIR_RESULT}/iperf3-stats"

# Read arguments
while [ "$1" != "" ]; do
    case $1 in
    	-s | --server)
            shift
            SERVER=$1
            ;;
    	-t | --time)
            shift
            TIME=$1
            ;;
        -p | --port)
            shift
            PORT=$1
            ;;
	esac
    shift
done

# Ensure the target server address was captured
if [ -z "${SERVER}" ]; then
    echo "Target server manditory!" >&2
    exit
fi

# Prepare working directory
mkdir -p "${DIR_WORK}"
STAMP=$( tr -cd 'a-zA-Z0-9' < /dev/urandom | head -c 8 )
LOG_FILE="${DIR_WORK}/${STAMP}"

eval "${IPERF} -c ${SERVER} -t ${TIME} -u -b${BANDWIDTH} -p ${PORT} -V" > "${LOG_FILE}"
