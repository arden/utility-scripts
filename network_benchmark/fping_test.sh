#!/bin/bash
#set -x

FPING_CMD=$(which fping)
if [[ -z "${FPING_CMD}" ]]; then
    FPING_CMD="/usr/sbin/fping"
fi

# Parameters for mtr
COUNT=300
TARGET=""


while [ "$1" != "" ]; do
    case $1 in
    	-t | --target)      shift
			                TARGET=$1
                            ;;
    	-c | --count)      shift
			                COUNT=$1
                            ;;
	esac
    shift
done

# Set file and directory names
DIR_RESULT="./result"
mkdir -p "${DIR_RESULT}"
FILE_RESULT="${DIR_RESULT}/fping_$(date +"%Y%m%d%H%M").log"

# Run the test and store the result

${FPING_CMD} -q -e -c "${COUNT}" <"${TARGET}" > "${FILE_RESULT}" 2>&1
