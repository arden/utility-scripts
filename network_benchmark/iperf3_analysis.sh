#!/bin/bash

HOST=$(hostname)
TIMESTAMP=$(date +"%Y-%m-%d_%H%M%S")

find "./$1" -type f | xargs awk -f ./iperf3_analysis.awk >> "${HOST}_${TIMESTAMP}.tsv"
