#!/usr/bin/awk

/Time: / {

    time = substr( $0, 6 )

    }

/] local/ {

    client = $4
    server = $9

    }

/0.00-180.00 sec/ {

    transfer = $5
    bandwidth = $7
    jitter = $9
    
    split( $11, counts, "/" )

    lost = counts[1]
    total = counts[2]

    }

/iperf Done./ {

    OFS="\t"

    print time, client, server, transfer, bandwidth, jitter, lost, total

    }
