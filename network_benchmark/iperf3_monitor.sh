#!/bin/bash

IPERF=$(which iperf3)

# Default parameters for iperf
TIME=17
STREAMS=8
OMIT=1

# Defaults
TIMESTAMP=$(date -u --rfc-3339=seconds)
DIR_RESULT="iperf3_mon"
STAT_FILE="${DIR_RESULT}/stats"
mkdir -p "${DIR_RESULT}"

# Ensure the target client address was captured
if [ ! -z $1 ]; then
    TARGET=$1
else
    echo "Target server manditory!" >&2
    exit
fi

# Prepare working directory
STAMP=$( tr -cd 'a-zA-Z0-9' < /dev/urandom | head -c 8 )
WORK_DIR="${DIR_RESULT}/${STAMP}"
mkdir -p "${WORK_DIR}"
SEND_FILE="${WORK_DIR}/send"
RECV_FILE="${WORK_DIR}/recv"

# Store the send
eval "${IPERF} -c ${TARGET} -t ${TIME} -P ${STREAMS} -O ${OMIT} -V" > "${SEND_FILE}"

# Store the receive
eval "${IPERF} -c ${TARGET} -t ${TIME} -P ${STREAMS} -O ${OMIT} -R -V" > "${RECV_FILE}"

RECV_SPEED=$(awk '/\[SUM\]/ { sum_speed = $6; sum_unit = $7 }; END { print sum_speed, sum_unit }' "${RECV_FILE}")
SEND_SPEED=$(awk '/\[SUM\]/ { sum_speed = $6; sum_unit = $7 }; END { print sum_speed, sum_unit }' "${SEND_FILE}")

printf "%s\t%s\t%s\t%s\n" "${TIMESTAMP}" "${STAMP}" "${RECV_SPEED}" "${SEND_SPEED}" >> "${STAT_FILE}"
