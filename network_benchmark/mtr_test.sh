#!/bin/bash

MTR_CMD=$(which mtr)

# Parameters for mtr
COUNT=10

# Ensure that the target address was captured
if [ ! -z "$1" ]; then
    TARGET=$1
else
    echo "Target server manditory!" >&2
    exit
fi

# Set file and directory names
SHORT_NAME=${TARGET%%.*}
DIR_RESULT="${SHORT_NAME}_mtr_data/work"
FILE_RESULT="${SHORT_NAME}_$(date +"%Y%m%d%H%M").log"
STAT_RESULT="${SHORT_NAME}_mtr_data/${SHORT_NAME}_stats"

# Create the directory
mkdir -p "${DIR_RESULT}"

# Run the test and store the result
eval "${MTR_CMD} -r -c ${COUNT} ${TARGET}" > "${DIR_RESULT}/${FILE_RESULT}"

# Parse the relevent info from the results
eval grep "${SHORT_NAME}" "${DIR_RESULT}/*" | awk -v OFS="," '{ print $4, $7 }' > "${STAT_RESULT}"
