#!/bin/bash
#set -x

BASE=$(pwd)

SOURCE_WHITESPACE=$(hostname -I)
SOURCE="$(echo -e "${SOURCE_WHITESPACE}" | sed -e 's/[[:space:]]*$//')"

FILENAME="${SOURCE}_results.tsv"
rm "$FILENAME"
cd result

#RESULT_FILES=$(ls)

for file in $(ls); do
    #read in the individual lines of each file
    while IFS='' read -r line || [[ -n "$line" ]]; do

        echo "$line" | awk -v "source=$SOURCE" -v "filename=$file" \
        '/^192.168/{
            OFS="\t"
            split ( $5, packet_counts, "/");
            stats_count=split ( $8, stats, "/");
            if (stats_count < 3){
                split ( "0 0 0", stats, " ");
            }
            split ( filename, head_trim, "_");
            split ( head_trim[2], tail_trim, ".");

            print tail_trim[1], source, $1, packet_counts[1], packet_counts[2], packet_counts[3], stats[1], stats[2], stats[3]
        }' >> ../"$FILENAME"


    done < "$file"
    #move to the next file
done

cd "$BASE"
