#!/bin/bash
# Restore Script for MaxDB intended to sync a warm standby
# Intended to be run as the <sid>adm user from the
# <sid>adm/$HOME/lc_standby/ directory
# 
#
# Features:
# * Compares copied version of dbm.ebf to live dbm.ebf file
# * If change is detected it will execute the appropriate restore
# * Supports Data and Log restores
#
# ToDo:
# * Add check to confirm DB connection works
# * Rework lc_standby directory config

# Usage --------------------------------------------
USAGE="MaxDB restore script.  Should be run as <sid>adm user.
Usage: 

Required Arguments:
    --db-sid            The System ID of the MaxDB database

Optional Arguments:
    --dbm-ebf-file      The path to the live dbm.ebf file
    --dbm-recovery-script The path to the dbm recovery script
    --db-creds-file     The path to the database credentials file
    --dry-run           Recovery script is generated but not run
    --help              Display this help and exit
"

# Set default values
DBM_RECOVERY_SCRIPT="${HOME}/lc_standby/recovery-script.dbm"
DB_CREDS_FILE="${HOME}/lc_standby/db-creds"
APPLIED_DBM_EBF_FILE="${HOME}/lc_standby/applied_dbm.ebf"
LIVE_EBF_FILE=""
DRY_RUN=false
DB_CREDS=""

function parse_arguments()
{
    #-------- Check and Parse Arguments -------------------------------------------
    # Parse parameters
    while [[ $# -gt 0 ]]
    do
        KEY="${1}"
        case $KEY in
            --db-sid)
                DB_SID="${2}"
                shift
                shift
                ;;
            --dbm-ebf-file)
                LIVE_EBF_FILE="${2}"
                shift
                shift
                ;;
            --dbm-recovery-script)
                DBM_RECOVERY_SCRIPT="${2}"
                shift
                shift
                ;;
            --db-creds-file)
                DB_CREDS_FILE="${2}"
                shift
                shift
                ;;
            --dry-run)
                DRY_RUN=true
                shift
                ;;
            --help)
                echo "${USAGE}"
                exit 0
                ;;
            *)
                echo "Unknown parameter: ${1}"
                echo "${USAGE}"
                exit 1
                ;;
        esac
    done

    # Check if the mandatory parameter is provided
    if [ -z "${DB_SID}" ]; then
        echo "Error: --db-sid parameter is required"
        echo "${USAGE}"
        exit 1
    fi

    # Set the current EBF history file
    LIVE_EBF_FILE="/sapdb/data/wrk/${DB_SID}/dbm.ebf"

    #-------- Verify Environment --------------------------------------------------
    # Command Definitions
    TOUCH=$(command -v touch)
    TRUNCATE=$(command -v truncate)
    GREP=$(command -v grep)
    TAIL=$(command -v tail)
    CUT=$(command -v cut)
    SED=$(command -v sed)
    CP=$(command -v cp)
    DBMCLI=$(command -v dbmcli)

    # Ensure all required binaries are present
    if ! [ -x "${TOUCH}" ]; then
        echoerr "'touch' is required!"
    elif ! [ -x "${TRUNCATE}" ]; then
        echoerr "'truncate' is required!"
    elif ! [ -x "${GREP}" ]; then
        echoerr "'grep' is required!"
    elif ! [ -x "${TAIL}" ]; then
        echoerr "'tail' is required!"
    elif ! [ -x "${CUT}" ]; then
        echoerr "'cut' is required!"
    elif ! [ -x "${SED}" ]; then
        echoerr "'sed' is required!"
    elif ! [ -x "${CP}" ]; then
        echoerr "'cp' is required!"
    elif ! [ -x "${DBMCLI}" ]; then
        echoerr "'dbmcli' is required!"
    fi

    # Check if required directories/files actually exist
    if [ ! -r "${LIVE_EBF_FILE}" ]; then
        echo "Error: ${LIVE_EBF_FILE} not found."
        echo "Or user does not have access"
        echo "File is likely similar to /sapdb/data/wrk/<SID>/dbm.ebf."
        exit 1
    fi

    if [ ! -r "${DB_CREDS_FILE}" ]; then
        echo "Error: ${DB_CREDS_FILE} not found"
        echo "Or user does not have access"
        echo "Default location is ${HOME}/lc_standby/db-creds."
        exit 1
    fi

    # Read db creds file
    DB_CREDS=$(cat "${DB_CREDS_FILE}")
}

# Will check that it can connect to db
function check_db()
{

    DB_STATE=$("${DBMCLI}" -d "${DB_SID}" -u "${DB_CREDS}" -c "db_state" | grep -q "ONLINE"; echo $?)

        if [ "${DB_STATE}" -ne 0 ]; then

            echo "Database is not online." | mail -s "${HOSTNAME} MaxDB Restore Failed" broyse@ardencompanies.com
            exit
        fi

}

function prepare_recovery()
{

    # Ensure Recovery script is empty and exists
    if [ -e "${DBM_RECOVERY_SCRIPT}" ]; then
        "${TRUNCATE}" -s 0 "${DBM_RECOVERY_SCRIPT}"
    else
        "${TOUCH}" "${DBM_RECOVERY_SCRIPT}"
    fi

    # Check for an applied ebf file
    # If none exists create a blank one
    if [ ! -e "${APPLIED_DBM_EBF_FILE}" ]; then
        echo "${APPLIED_DBM_EBF_FILE} file not found"
        echo "Assuming first run"
        echo "Creating initial applied_dbm.ebf"

        "${TOUCH}" "${APPLIED_DBM_EBF_FILE}"
    fi

    #DEBUG Check variables
    echo "Setting DB SID to ${DB_SID}"
    echo "Setting path for live dbm.ebf file to ${LIVE_EBF_FILE}"
    echo "Setting path for dbm recovery script to ${DBM_RECOVERY_SCRIPT}"
    echo "Setting DB credentials to REDACTED"

    # Find the last DATA backupid
    LAST_DATA_BACKUP=$(cat "${LIVE_EBF_FILE}" | awk -F"|" '$4 ~ /DATA/ { print $3 }' | tail -n 1)

    # Find the last LOG backupid 
    LAST_LOG_BACKUP=$(cat "${LIVE_EBF_FILE}" | awk -F"|" '$4 ~ /LOG/ { print $3 }' | tail -n 1)

    LAST_LOG_BACKUP_DEPENDENCY=$(cat "${LIVE_EBF_FILE}" | awk -F"|" '$4 ~ /LOG|DATA/ { print $3 }' | tail -n 2 | awk 'NR!=2')
    # Find the LOG backupids after the last DATA backup
    # These will need to be restored after the DATA backup
    REQ_LOG_BACKUP=$(cat "${LIVE_EBF_FILE}" | awk -v lastbk="${LAST_DATA_BACKUP}" -F"|" 'BEGIN { print_logs=0 } $0 ~ lastbk { print_logs=1 } $4 ~ /LOG/ && print_logs == 1 { print $3 }')

    # Find the last applied DATA and LOG backups
    LAST_APPLIED_BACKUP=$(cat "${APPLIED_DBM_EBF_FILE}" | awk -F"|" '$4 ~ /DATA/ { print $3 }' | tail -n 1)
    LAST_APPLIED_LOG=$(cat "${APPLIED_DBM_EBF_FILE}" | awk -F"|" '$4 ~ /LOG/ { print $3 }' | tail -n 1)

    # Initialize as false
    DATA_RESTORE=false
    LOG_RESTORE=false

    # Check if last applied data backup has been applied
    if [ "${LAST_DATA_BACKUP}" != "${LAST_APPLIED_BACKUP}" ]; then
        DATA_RESTORE=true
    # If data restore isnt needed, check if log restore is.
    elif [ "${LAST_LOG_BACKUP}" != "${LAST_APPLIED_LOG}" ]; then
        LOG_RESTORE=true
    fi

    # Do a data restore if required
    if [ "${DATA_RESTORE}" = true ]; then
        #DEBUG Backups needed
        echo "Data Restore Backup ID ${LAST_DATA_BACKUP} is being restored"
        echo "Required Logs are:" 
        echo "${REQ_LOG_BACKUP}"
        
        data_restore "${REQ_DATA_BACKUP}" "${REQ_LOG_BACKUPS}"


    # What about a log restore?
    elif [ "${LOG_RESTORE}" = true ]; then
        #DEBUG Backups needed
        echo "Log Restore Backup ID ${LAST_DATA_BACKUP}"
        echo "Log Restore Dependency Backup ID ${LAST_LOG_BACKUP_DEPENDENCY}"

        NUM_LOG_BACKUPS=$(echo "${REQ_LOG_BACKUP}" | wc -l)

        if [ "${NUM_LOG_BACKUPS}" -gt 1 ]; then
            log_multi_restore "${REQ_LOG_BACKUPS}" "${LAST_DATA_BACKUP}"
        else
            log_single_restore "${LAST_LOG_BACKUP}" "${LAST_LOG_BACKUP_DEPENDENCY}"
        fi
        
    fi
}

function data_restore()
{
    REQ_DATA_BACKUP=$1
    REQ_LOG_BACKUPS=$2

    # Build the commands to restore the last data backup
        {
            echo "db_admin"
            # dbmcli can't handle single quotes
            echo "db_activate RECOVER BACKData2 DATA externalbackupid \"${LAST_DATA_BACKUP}\""
            echo "db_start"
        } >> "${DBM_RECOVERY_SCRIPT}"

        NUM_LOG_BACKUPS=$(echo "${REQ_LOG_BACKUP}" | wc -l)
    
        # Build commands to restore associated log backups if they exist
        case "${NUM_LOG_BACKUPS}" in
            0)
                # No logs so complete
                echo "Restore Complete"
            ;;
            1)
                {
                # Add the commands required for a single log restore
                echo "recover_start backlog log externalbackupid ${REQ_LOG_BACKUP}"
                echo "recover_cancel" 
                echo "db_start" 
                } >> "${DBM_RECOVERY_SCRIPT}"
            ;;
            *)
                # Add the commands required for multi log restore
                LOG_DBM_COMMANDS=$(echo -e "${REQ_LOG_BACKUP}" | \
                    "${SED}" -e "1s/^/recover_start BackLog ExternalBackupID \"/" \
                            -e "2,\$s/^/recover_replace BackLog ExternalBackupID \"/" \
                            -e 's/$/"/'
                )

                {
                echo "${LOG_DBM_COMMANDS}" 
                echo "recover_cancel" 
                echo "db_start"
                } >> "${DBM_RECOVERY_SCRIPT}"
            ;;
        esac
}

# Multi log restore - should work - needs testing
function log_multi_restore()
{
    REQ_LOG_BACKUPS=$1
    LAST_DATA_BACKUP=$2

    LOG_DBM_COMMANDS=$(echo -e "${REQ_LOG_BACKUP}" | \
        "${SED}" -e "1s/^/recover_start BackLog ExternalBackupID \"/" \
                 -e "2,\$s/^/recover_replace BackLog ExternalBackupID \"/" \
                 -e 's/$/"/'
    )
                {
                echo "${LOG_DBM_COMMANDS}"
                echo "recover_cancel"
                echo "db_start"
                } >> "${DBM_RECOVERY_SCRIPT}"
    
}

# Single log only restore - should work - needs testing
function log_single_restore()
{
    REQ_LOG_BACKUPS=$1
    LAST_LOG_BACKUP_DEPENDENCY=$2
    
        {
        echo "recover_start ${LAST_LOG_BACKUP_DEPENDENCY}"
        echo "recover_replace" ${REQ_LOG_BACKUP} 
        echo "recovery_cancel"
        echo "db_start"
        } >> "${DBM_RECOVERY_SCRIPT}"

}

# Restore Function
function start_restore()
{
        # Actually run the restore script
        "${DBMCLI}" -d "${DB_SID}" -u "${DB_CREDS}" -uUTL -i "${DBM_RECOVERY_SCRIPT}"
        RC=$?

        # If succesful copy ebf file
        if [ "${RC}" -ne 0 ]; then

            echo $RC > Return_Code.txt
            echo "Return codes attached." | mail -s "${HOSTNAME} MaxDB Restore Failed" -a $RC broyse@ardencompanies.com
        else
            "${CP}" "$LIVE_EBF_FILE" "$APPLIED_DBM_EBF_FILE"
        fi
}

#-------- Main Control Block --------------------------------------------------
#shellcheck disable=SC2068
parse_arguments $@

# Still needs work
#check_db

prepare_recovery

if [ "${DRY_RUN}" = false ]; then
{
    start_restore
}
fi