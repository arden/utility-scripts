#!/bin/bash

while [[ $# -gt 0 ]]
do
key="$1"

# Parse parameters
case $key in
    -l|--log-only)
    log_only="$2"
    shift
    shift
    ;;
    -s|--db-sid)
    db_sid="$2"
    shift
    shift
    ;;
    -c|--cred-file)
    db_creds_file="$2"
    shift
    shift
    ;;
    *)
    echo "Unknown parameter: $1"
    exit 1
    ;;
esac
done

if [ -z "$db_sid" ]
then
    echo "Database SID is not specified. Use -s or --db-sid option to specify."
    exit 1
fi

# Check if cred file exists
if [ ! -e "$db_creds_file" ]; then
    echo "Error: $db_creds_file not found, please populate."
    echo "Default location is $HOME/lc_standby/db-creds."
    exit 1
fi

# Read db creds file
db_creds=$(cat "$db_creds_file")

# Run appropriate mode
if [ -z "$log_only" ]
then
    echo "No backup type specified, assuming full backup is required. Use -l (--log-only) option to run only a log backup"
    echo "Initiating data backup for database SID: $DB_SID"
    dbmcli -d "$DB_SID" -u "$db_creds" -uUTL -c backup_start BackData2
else
    echo "Initiating log backup for database SID: $DB_SID."
    dbmcli -d "$DB_SID" -u "$db_creds" -uUTL -c backup_start BackLog
fi
