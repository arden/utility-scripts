#!/bin/bash

# Configuration
DB2_CMD="/db2/db2sdv/sqllib/bin/db2"
DB_NAME="SDV"
BACKUP_PATH="/srv/backups/db2/SDV/"
LOG_FILE="/srv/backups/db2/SDV/backup_logs/backup_$(date +%Y%m%d).log"
DATE=$(date +%Y%m%d_%H%M%S)
EMAIL_RECIPIENT="broyse@ardencompanies.com"
DRY_RUN=false  # Default to normal execution

# Helper function for logging
log_message() {
    local MESSAGE=$1
    echo "$(date +'%Y-%m-%d %H:%M:%S') - $MESSAGE" | tee -a "$LOG_FILE"
}

# Function to send an email notification
send_notification() {
    local SUBJECT=$1
    local MESSAGE=$2
    if $DRY_RUN; then
        log_message "[Dry Run] Skipping email notification: $SUBJECT"
    else
        echo "$MESSAGE" | mail -s "$SUBJECT" "$EMAIL_RECIPIENT"
    fi
}

# Function to check database connection
check_connection() {
    log_message "Checking database connection for $DB_NAME..."
    if $DRY_RUN; then
        log_message "[Dry Run] Simulating database connection check for $DB_NAME."
    else
        $DB2_CMD connect to $DB_NAME >> "$LOG_FILE" 2>&1
        if [[ $? -eq 0 ]]; then
            log_message "Database connection successful for $DB_NAME."
            $DB2_CMD disconnect $DB_NAME >> "$LOG_FILE" 2>&1
        else
            log_message "ERROR: Unable to connect to database $DB_NAME."
            send_notification "DB2 Backup Failure: Connection Error" "Failed to connect to database $DB_NAME on $DATE. Check logs at $LOG_FILE."
            exit 1
        fi
    fi
}

# Function to perform a full backup
full_backup() {
    check_connection
    log_message "Starting full online backup for $DB_NAME..."
    if $DRY_RUN; then
        log_message "[Dry Run] Simulating full online backup: $DB2_CMD backup database $DB_NAME online to $BACKUP_PATH with 2 buffers buffer 2048 compress"
    else
        $DB2_CMD backup database $DB_NAME online to $BACKUP_PATH with 2 buffers buffer 2048 compress >> "$LOG_FILE" 2>&1
        if [[ $? -eq 0 ]]; then
            log_message "Full backup completed successfully for $DB_NAME."
            send_notification "DB2 Backup Success: Full Backup" "Full backup for $DB_NAME completed successfully on $DATE."
        else
            log_message "ERROR: Full backup failed for $DB_NAME."
            send_notification "DB2 Backup Failure: Full Backup" "Full backup for $DB_NAME failed on $DATE. Check logs at $LOG_FILE."
            exit 1
        fi
    fi
}

# Function to perform an incremental backup
incremental_backup() {
    check_connection
    log_message "Starting incremental online backup for $DB_NAME..."
    if $DRY_RUN; then
        log_message "[Dry Run] Simulating incremental online backup: $DB2_CMD backup database $DB_NAME online incremental to $BACKUP_PATH compress"
    else
        $DB2_CMD backup database $DB_NAME online incremental to $BACKUP_PATH compress >> "$LOG_FILE" 2>&1
        if [[ $? -eq 0 ]]; then
            log_message "Incremental backup completed successfully for $DB_NAME."
            send_notification "DB2 Backup Success: Incremental Backup" "Incremental backup for $DB_NAME completed successfully on $DATE."
        else
            log_message "ERROR: Incremental backup failed for $DB_NAME."
            send_notification "DB2 Backup Failure: Incremental Backup" "Incremental backup for $DB_NAME failed on $DATE. Check logs at $LOG_FILE."
            exit 1
        fi
    fi
}

# Function to archive logs
archive_logs() {
    check_connection
    log_message "Archiving logs for $DB_NAME..."
    if $DRY_RUN; then
        log_message "[Dry Run] Simulating log archiving: $DB2_CMD archive log for database $DB_NAME"
    else
        $DB2_CMD archive log for database $DB_NAME >> "$LOG_FILE" 2>&1
        if [[ $? -eq 0 ]]; then
            log_message "Log archiving completed successfully for $DB_NAME."
            send_notification "DB2 Log Archive Success" "Log archiving for $DB_NAME completed successfully on $DATE."
        else
            log_message "ERROR: Log archiving failed for $DB_NAME."
            send_notification "DB2 Log Archive Failure" "Log archiving for $DB_NAME failed on $DATE. Check logs at $LOG_FILE."
            exit 1
        fi
    fi
}

# Parse dry-run argument
if [[ "$1" == "dry-run" ]]; then
    DRY_RUN=true
    shift  # Remove "dry-run" from arguments
    log_message "[Dry Run] Dry run mode enabled. No changes will be made."
fi

# Main script
case $1 in
    full)
        full_backup
        ;;
    incremental)
        incremental_backup
        ;;
    archive_logs)
        archive_logs
        ;;
    *)
        echo "Usage: $0 [dry-run] {full|incremental|archive_logs}"
        exit 1
        ;;
esac

# End of script
