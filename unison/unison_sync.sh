#!/bin/bash
# Unision Sync Trigger =============================
#
# This script is a wrapper to trigger unison sync
# profiles
#
# Local Constants ----------------------------------
IPV4_PATTERN='\b((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.|$)){4}\b'
EMAIL_PATTERN='\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}\b'

# File and Directory Paths -------------------------
CWD=$(pwd)

# Usage --------------------------------------------
USAGE="Simple Unison Wrapper
Usage: [-a ALIAS] PROFILE
   or: [-h]

Executes a unsion sync with the specified profile.

Optional Arguments:
  -a CONNECTION_ALIAS If specified, the script runs only when the listed
                      alias is present on the server. Currently only
                      IPv4 address and DNS names are supported.
  -h, --help          display this help and exit
"

#-------- Skip if IP Alias is Missing -----------------------------------------
function check_ip_alias()
{
    local ALIAS=$1
    # Do nothing if the alias is false
    if [ "${ALIAS}" == 'false' ]; then
        return
    fi

    if ! "${IP_CMD}" -4 addr | grep "${ALIAS}" >/dev/null; then
        echo "alias '${ALIAS}' not present, skipping!"
        exit 0
    fi
}

#-------- Print Error Message -------------------------------------------------
function echoerr()
{
    printf "%s\n" "$*" >&2;
    exit 1
}

#-------- Parse Arguments -----------------------------------------------------
# Shamelessly stolen from
# https://medium.com/@Drew_Stokes/bash-argument-parsing-54f3b81a6a8f
# Note that the variables in this function are intentionally globals
function parse_arguments()
{
    # Collect parameters
    PARAMS=""
    while (( "$#" ));
    do
        case "$1" in
            -h|--help)
                # Print usage and quit
                echo "${USAGE}"
                exit 0
                ;;
            -a)
                CONNECTION_ALIAS=$2
                shift 2
                ;;
            --) # end argument parsing
                shift
                break
                ;;
            -*) # unsupported flags
                echoerr "Unsupported flag ${1}"
                ;;
            *) # preserve positional arguments
                PARAMS="$PARAMS $1"
                shift
                ;;
        esac
    done

    # Record positional parameters (there are none)
    eval set -- "${PARAMS}"
    PROFILE="$1"

    # Check mandatory parameters
    if [ -z "${PROFILE}" ]; then 
        echoerr "PROFILE is mandatory!"
    fi
}

#-------- Verify Environment --------------------------------------------------
function verify_environment()
{
    # Fix the path / ld_library_path
    if ! [[ "${PATH}" =~ (^|:)/usr/local/bin(:|$) ]]; then
        export PATH="/usr/local/bin:${PATH}"
    fi

    # Ensure /usr/sbin is in the path (ip is there)
    if ! [[ "${PATH}" =~ (^|:)/usr/sbin(:|$) ]]; then
        export PATH="/usr/sbin:${PATH}"
    fi

    # Ensure `/lib` is present in LD_LIBRARY_PATH
    if [ -z "${LD_LIBRARY_PATH}" ]; then
        export LD_LIBRARY_PATH='/lib'
    else
        export LD_LIBRARY_PATH="/lib:${LD_LIBRARY_PATH}"
    fi

    # Command Definitions
    GETENT=$(command -v getent)
    IP_CMD=$(command -v ip)
    UNISON=$(command -v unison)

    # Ensure all required binaries are present
    if ! [ -x "${IP_CMD}" ]; then
        echoerr "'ip' is required!"
    elif ! [ -x "${GETENT}" ]; then
        echoerr "'getent' is required!"
    elif ! [ -x "${UNISON}" ]; then
        echoerr "'unison' is required!"
    fi

    # Ensure we have either an IPv4 address or a hostname that can be resolved
    # to one
    if [ -z "${CONNECTION_ALIAS}" ]; then
        IP_ALIAS='false'
    elif [[ "${CONNECTION_ALIAS}" =~ $IPV4_PATTERN ]]; then
        IP_ALIAS="${CONNECTION_ALIAS}"
    elif RESOLVE="$("${GETENT}" hosts "${CONNECTION_ALIAS}")"; then
        IP_ALIAS=$(echo "${RESOLVE}" | awk '{print $1}')
    else
        echoerr "Invalid IP_ALIAS: '${IP_ALIAS}'"
    fi
}

#======== Main control block ==================================================

# Record the parameters - intentionally multiple words
# shellcheck disable=SC2068
parse_arguments $@

# Verify Environment
verify_environment

# Check for the IP Alias
check_ip_alias "${IP_ALIAS}"

unison $PROFILE
