#!/usr/bin/ksh
FILE_QUEUE="/etc/qconfig"

# Simple command to output printer DNS names
awk '/backend/ { if ( $1 !~ "[*]" ) { print $4 } }' \
    "${FILE_QUEUE}" > /tmp/printer-list

# More advanced processing could be done
