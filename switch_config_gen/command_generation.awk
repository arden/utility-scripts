#!/usr/bin/awk

BEGIN {
    FS="\t"
    
    bits = 8
    for(x = 255; x >=0; x -= 2^i++)
        cidr[bits--] = x
}

# Print the relevant info for lines
# starting with the correct location
$1 ~ location {
    # vlan, name, subnet, class
    vlan=$2
    name=$3
    subnet=$4
    class=$5
    routed=$7
    dhcp=$8

    #print vlan, name, subnet, class

    vlans[vlan] = name":"subnet":"class":"routed":"dhcp

    #print vlans[vlan]
}

END {
    vlan_count = asorti(vlans, vlan_id, "@ind_num_asc")

    # vlan name
    for( i = 1; i <= vlan_count; i++ )
    {
        split( vlans[vlan_id[i]], vlan_data, ":" )
        printf "vlan %i\n  name %s\n", vlan_id[i], vlan_data[1]
    }
    
    # vlan interface
    for( i = 1; i <= vlan_count; i++ )
    {
        split( vlans[vlan_id[i]], vlan_data, ":" )
        m_oct = int(vlan_data[3] / 8)
        l_oct = vlan_data[3] % 8 
        for( octi = 0; octi < 4; octi++ )
        {
            if( m_oct > 0 )
                oct[octi] = cidr[8]
            else if( m_oct == 0 )
                oct[octi] = cidr[l_oct]
            else
                oct[octi] = "0"

            m_oct--
        }
        
        split( vlan_data[2], gw_ip, "." )
        
        # Only issue output if this interface is routed
        if( vlan_data[4] ~ "Yes" )
        {
            printf "interface vlan %i\n  ip address %i.%i.%i.%i %i.%i.%i.%i\n", \
                vlan_id[i], gw_ip[1], gw_ip[2], gw_ip[3], gw_ip[4] + 1, \
                oct[0], oct[1], oct[2], oct[3]

            if( vlan_data[5] ~ "Yes" )
                printf "  ip helper-address %s\n", dhcp_helper
        }
    }
}
