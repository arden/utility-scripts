#!/usr/bin/ksh

TRUE=1
FALSE=0
EXECUTE_FLAG=$FALSE

if [[ $# -lt 1 ]]
then
    echo "error: $# is too few, at least a filter is required!"
    exit 1
fi

LUN_NAME_FILTER=$1

if [[ ! -z $2 ]]
then
    EXECUTE_FLAG=$TRUE
fi

for DISK in $(sanlun lun show | grep -v "${LUN_NAME_FILTER}" | awk '$8 ~ /GOOD/ { print $3 }')
do
    lspv | grep None | grep -w "${DISK}" >/dev/null 2>&1 
    if [[ $? -eq 0 ]]
    then
        if [[ $EXECUTE_FLAG -eq $TRUE ]]
        then
            rmdev -d -l "${DISK}" -R
        else
            echo "rmdev -d -l ${DISK} -R"
        fi
    fi
done

