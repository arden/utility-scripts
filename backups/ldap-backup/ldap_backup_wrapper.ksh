#!/usr/bin/ksh
# This script remotes into a server (passed through argument) and then executes the ldap_backup.ksh script.
# That script then creates a backup of the openLDAP database and encodes it using openSSL.
# Afterwards, this script moves the new backup into /backup/ldap, where crashplan automatically handles things.

#set -x

# Variables 
REMOTEHOST=
BACKUPDIR=/tmp/ldap
ARCHIVEDIR=/usr/openldap/db/ardencompanies.com
DESTDIR=/backup/ldap
BAK_RETENTION=7
ARCH_RETENTION=30

CODE_SUCCESS=0
CODE_WARNING=1
CODE_CRITICAL=2

# Get the Arguments
if [ "$1" = "" ]; then
    echo "You don't have any flags. Include a hostname that hosts the openLDAP server."
    exit 0
fi

while [ "$1" != "" ]; do
    case "$1" in
        -h | --host | --hostname )      shift
                                        REMOTEHOST=$1
    esac
    shift
done

# Create the Commands
BACKUP_SCRIPT=/home/rescue/ldap_backup.ksh
BAK_PREFIX=ldap-nix
ARCH_PREFIX=ldap-db-archive

BACKUP_CMD="ssh root@$REMOTEHOST $BACKUP_SCRIPT"
$BACKUP_CMD
BACKUP_RETURN=${?}

if [ $BACKUP_RETURN -ne 0 ]
then
    echo "LDAP Backup Script has encountered a problem creating the backup archive." | mail -s "Backup Unsuccessful: $REMOTEHOST openLDAP" backups@ardencompanies.com
    exit $CODE_CRITICAL
fi

# Now that the database is backed up, we can begin the cleanup process on the old logs.
ARCHIVE_SCRIPT=/home/rescue/archive_logs.ksh
ARCHIVE_CMD="ssh root@$REMOTEHOST $ARCHIVE_SCRIPT"
$ARCHIVE_CMD
ARCHIVE_RETURN=${?}

if [ $ARCHIVE_RETURN -ne 0 ]
then
    echo "LDAP Log Archival cleanup has encountered a problem." | mail -s "Log Cleanup Unsuccessful: $REMOTEHOST openLDAP" backups@ardencompanies.com
fi

# SCP over the the database backup.
BAK_SCP_CMD="scp root@$REMOTEHOST:$BACKUPDIR/ldap* /backup/ldap"
$BAK_SCP_CMD
BAK_SCP_RETURN=${?}

if [ $BAK_SCP_RETURN -ne 0 ]
then
    echo "LDAP Backup Script has encountered a problem using SCP to send the backups to /backup." | mail -s "Backup Unsuccessful: $REMOTEHOST openLDAP" backups@ardencompanies.com
    exit $CODE_CRITICAL
else
    echo "LDAP Backup Script completed successfully." | mail -s "Backup Successful: $REMOTEHOST openLDAP" backups@ardencompanies.com
fi

# SCP over the the archived logs.
ARC_SCP_CMD="scp root@$REMOTEHOST:$ARCHIVEDIR/ldap* /backup/ldap"
$ARC_SCP_CMD
ARC_SCP_RETURN=${?}

if [ $ARC_SCP_RETURN -ne 0 ]
then
    echo "LDAP Script has encountered a problem using SCP to send the archives  to /backup." | mail -s "Archive Unsuccessful: $REMOTEHOST openLDAP" backups@ardencompanies.com
    exit $CODE_CRITICAL
else
    echo "LDAP Archival Script completed successfully." | mail -s "Archive Successful: $REMOTEHOST openLDAP" backups@ardencompanies.com
fi

# Removes backups & archives that are more than 1 month old from the destination directory.
find $DESTDIR/ -name "$BAK_PREFIX*" -type f -mtime +$BAK_RETENTION | xargs rm
find $DESTDIR/ -name "$ARCH_PREFIX*" -type f -mtime +$BAK_RETENTION | xargs rm
