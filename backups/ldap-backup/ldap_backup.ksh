#!/usr/bin/ksh
#set -x
# Constants
WRKDIR=/tmp
HOSTNAME=$(hostname -s)
PREFIX=ldap-$HOSTNAME
TIMESTAMP=$(date +'%Y-%m-%d-%H-%M')
PUBKEY=/home/rescue/ldap_public.pem
OUTFORM=DER
BACKUPDIR=/tmp/ldap
RETENTION=2

# Commands
OPENSSL=/opt/pware64/bin/openssl
SLAPCAT=/opt/pware64/sbin/slapcat
TAR=/opt/freeware/bin/tar

# Make sure the temporary directory exists
mkdir -p $WRKDIR/"$PREFIX"
mkdir -p $BACKUPDIR
# Go there
cd $WRKDIR/"$PREFIX"

# Output the backend database and the config database
$SLAPCAT -b "dc=ardencompanies,dc=com" > ./backend-"$TIMESTAMP".backup
$SLAPCAT -b "cn=config" > ./config-"$TIMESTAMP".backup

# Go to the main work directory
cd $WRKDIR

# tar and bzip2 the backup files to ease the encryption process
$TAR -cvjf $WRKDIR/$PREFIX-$TIMESTAMP.tar.bz2 $PREFIX

# Remove the source files
rm -rf $WRKDIR/$PREFIX

# Encrypt the files using the public key
$OPENSSL smime -encrypt -aes256 -in $WRKDIR/$PREFIX-$TIMESTAMP.tar.bz2 -binary -outform $OUTFORM -out $BACKUPDIR/$PREFIX-$TIMESTAMP.tar.bz2.crypt $PUBKEY

# Remove the source tarball
rm $WRKDIR/"$PREFIX-$TIMESTAMP.tar.bz2" 

# Remove backups older than the retention
find $BACKUPDIR/ -name "$PREFIX-*" -type f -mtime +$RETENTION | xargs rm
