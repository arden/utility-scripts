#!/usr/bin/ksh
#set -x

# Exit codes
CODE_SUCCESS=0
CODE_WARNING=1
CODE_CRITICAL=2

# Important variables
HOSTNAME=$(hostname -s)
TIMESTAMP=$(date +'%Y-%m-%d-%H-%M')
PREFIX=ldap-db-archive-$HOSTNAME
OUTFORM=DER
RETENTION=2
VALIDATION_ERRORS=0
GET_LOG_ERRORS=0
NEED_CLEANING=0
ARCHIVE_ERRORS=0

# File/folder locations
PUBKEY=/home/rescue/ldap_public.pem
LOG_FOLDER=/usr/openldap/db/ardencompanies.com

# Binary locations
DB_ARCHIVE=/opt/pware64/bin/db_archive
OPENSSL=/opt/pware64/bin/openssl
TAR=/opt/freeware/bin/tar

env_validate ()
{
    # General environment validation, let's make sure that all our paths and binaries exist
    if [[ ! -e $DB_ARCHIVE ]];
    then
        echo "DB_ARCHIVE Program not found!"
        VALIDATION_ERRORS=1
    fi

    if [[ ! -e $OPENSSL ]];
    then
        echo "OPENSSL Program not found!"
        VALIDATION_ERRORS=1
    fi

    if [[ ! -e $TAR ]];
    then
        echo "TAR Program not found!"
        VALIDATION_ERRORS=1
    fi

    if [[ ! -e $PUBKEY ]];
    then
        echo "Encryption public key not found!"
        VALIDATION_ERRORS=1
    fi
    if [[ ! -d "$LOG_FOLDER" ]];
    then
        # This means that the log folder doesn't exist.
        # In this case, the script is likely running on something that isn't an openLDAP host
        # and something is seriously wrong.
        echo "Could not find Log folder $LOG_FOLDER."
        VALIDATION_ERRORS=1
    fi

} #env_validate ENDFUNCTION

get_logs ()
{
    # If we've passed this test, basic environment validation is looking good.
    cd $LOG_FOLDER || exit

    FREE_LOGS=$($DB_ARCHIVE)
    DB_ARCHIVE_RETURN=${?}

    if [[ $DB_ARCHIVE_RETURN -lt 2 ]] && [[ -z "$FREE_LOGS" ]];
    then
        NEED_CLEANING=0
        GET_LOG_ERRORS=0
    elif [[ $DB_ARCHIVE_RETURN -lt 2 ]] && [[ ! -z "$FREE_LOGS" ]];
    then
        NEED_CLEANING=1
        GET_LOG_ERRORS=0
    elif [[ $DB_ARCHIVE_RETURN -gt 1 ]];
    then
        GET_LOG_ERRORS=1
    fi

    # ERROR HANDLING:
    # Normally the db_archive program simply returns a list of filenames.
    # However, there are a lot of situations where this won't be the case. If there is corruption in a file,
    # or if something is incorrectly formatted / unreadable for some reason, some issues might occur.
    # In this situation, the binary will return some garbage error messages.

    # Need to have a method to handle this situation where we don't get a perfect return. Don't want to randomly
    # process garbage data full of error messages.

    # To solve this problem, we will scan through the list of things returned by DB_ARCHIVE and check that the file exists.
    # If the files all exist, that means that we have gotten a real return.

    for x in $FREE_LOGS
    do
        if [[ ! -e $x ]];
        then
            echo "Could not find log file $x returned by DB_ARCHIVE"
            GET_LOG_ERRORS=1
        fi
    done # ends the array loop through the FREE_LOGS

} #get_logs ENDFUNCTION

perform_archive ()
{
    cd $LOG_FOLDER || exit
    # if the FREE LOGS variable isn't empty, then that means that we have some logs to clean up.
    # archive the logs
    $TAR cvfj $LOG_FOLDER/$PREFIX-$TIMESTAMP.tar.bz2 $FREE_LOGS
    TAR_RETURN=${?}

    # encrypt the files using the public key
    $OPENSSL smime -encrypt -aes256 -in $LOG_FOLDER/$PREFIX-$TIMESTAMP.tar.bz2 \
        -binary -outform $OUTFORM -out $LOG_FOLDER/$PREFIX-$TIMESTAMP.tar.bz2.crypt $PUBKEY
    SSL_RETURN=${?}

    # remove the old unencrypted tarball
    rm ./"$PREFIX-$TIMESTAMP.tar.bz2"
    REMOVE_TAR=${?}

    # Remove the old encrypted tarballs that are older than 2 iterations.
    find $LOG_FOLDER/ -name "$PREFIX-*" -type f -mtime +$RETENTION | xargs rm

    if [[ $TAR_RETURN -eq 0 ]] && [[ $SSL_RETURN -eq 0 ]] && [[ $REMOVE_TAR -eq 0 ]];
    then
        # This means that the Tar command, SSL command, and Tar removal have all succeeded.
        # This means we have the go-ahead to remove the free logs now.
        rm $FREE_LOGS
        REMOVE_LOGS=${?}
        if [[ $REMOVE_LOGS -eq 0 ]];
        then
            ARCHIVE_ERRORS=0
        else
            ARCHIVE_ERRORS=1
        fi
    else
        ARCHIVE_ERRORS=1
    fi

} # perform_archive ENDFUNCTION

# Functions are properly set up now. Time to execute them.
# public static void main ( String [] args ){

# Run the environment validation. This guarantees our directories and binaries exist.
env_validate
if [[ ! $VALIDATION_ERRORS -eq 0 ]];
then
    # This means that there was an environment validation error. Send email about error and quit.
    echo "Validation tests failed on $HOSTNAME. Make sure all your binaries and directories are in place." | \
        mail -s "Log cleanup critical error: Environment validation failed $HOSTNAME" backups@ardencompanies.com
    exit $CODE_CRITICAL
fi

# This runs the DB_ARCHIVE utility to see if any logs are archiveable.
get_logs
if [[ ! $GET_LOG_ERRORS -eq 0 ]];
then
    # This means that there was an error returned by the DB_ARCHIVE utility. We need to bail out.
    echo "DB_ARCHIVE utility returned error code on $HOSTNAME." | \
        mail -s "Log Cleanup Failure: DB_ARCHIVE errors $HOSTNAME" backups@ardencompanies.com
    exit $CODE_WARNING
fi

if [[ $NEED_CLEANING -eq 0 ]];
then
    echo "No old Berkely DB logs to cleanup." | \
        mail -s "Log Cleanup Report: $HOSTNAME no free logs openLDAP" backups@ardencompanies.com
    exit $CODE_SUCCESS
fi

# This actually performs the archiving function, compressing and removing the archiveable logs.
perform_archive
if [[ $ARCHIVE_ERRORS -eq 0 ]];
then
    # If there are no errors, send a report because we are in the clear.
    printf "Archiveable logs found on %s.\n The following logs have been moved to /backup/ldap: \n %s" "$HOSTNAME" "$FREE_LOGS" | \
        mail -s "Log Cleanup Report: $HOSTNAME Archival report openLDAP" backups@ardencompanies.com
    exit $CODE_SUCCESS
else
    # If there are errors, we should fail out and send an error message.
    echo "Errors found while trying to archive BerkelyDB logs on $HOSTNAME." | \
        mail -s "Log Cleanup Errors: $HOSTNAME" backups@ardencompanies.com
    exit $CODE_CRITICAL
fi
