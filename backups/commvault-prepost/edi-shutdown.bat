@echo off
>c:\utility-scripts\edi-shutdown-output.txt (
	Powershell -ExecutionPolicy bypass -Command  "& 'c:\utility-scripts\backups\commvault-prepost\pre-shutdownvms.ps1' -vmlist 'dev-edi-db,dev-edi-app'"
)