[CmdletBinding()]
#Let's pull down our arguments passed to the script.
#Argument format is pre-shutdownvms.ps1 -vmlist vm1,vm2,vm3,etc
param 
(
      [Parameter(Mandatory = $true)]  [string] $vmlist
    , [Parameter(Mandatory = $false)] [int]    $job
    , [Parameter(Mandatory = $false)] [int]    $attempt
    , [Parameter(Mandatory = $false)] [int]    $bkplevel
)

Import-Module VMware.VimAutomation.Core
#Import the required powershell modules and such
#Set-PowerCLIConfiguration -InvalidCertificateAction ignore -confirm:$false

#pop this line in here if you don't have a credentials file to pull the login info for
#New-VICredentialStoreItem -host 'vcenter00.ardencompanies.com' -user 'arden\service-commvault' -password 'boy i'm sure not gonna commit this password in git' -file C:\utility-scripts\backups\commvault-prepost\in.creds

#Pull the hashed credentials out of your file you made
$creds = Get-VICredentialStoreItem -file  C:\utility-scripts\backups\commvault-prepost\in.creds

#Connect up to vsphere
Connect-VIServer -Server $creds.host -user $creds.user -Password $creds.password

set errlev=$lastexitcode
Echo Connectlevel=[$errlev]

$vmsplit = $vmlist.Split(",")

foreach ($vmname in $vmsplit){
    Get-VM $vmname | Shutdown-VMGuest -Confirm:$false
    set errlev=$lastexitcode
    Echo Shutdownlevel=[$errlev]
}

return 0