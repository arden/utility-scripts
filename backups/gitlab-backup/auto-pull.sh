#!/usr/bin/bash

DIR_REPO=$1

cd "${DIR_REPO}"

for REPO in $(find . -maxdepth 1 -mindepth 1 -type d)
do
    pushd "${REPO}"
    git pull --all
    popd
done
