#!/bin/bash

SOURCE=$1

convert -resize x16 -gravity center -crop 16x16+0+0 -flatten -colors 256 $1 \
    output-16x16.ico
convert -resize x32 -gravity center -crop 32x32+0+0 -flatten -colors 256 $1 \
    output-32x32.ico
convert -resize x48 -gravity center -crop 48x48+0+0 -flatten -colors 256 $1 \
    output-48x48.ico
convert -resize x64 -gravity center -crop 64x64+0+0 -flatten -colors 256 $1 \
    output-64x64.ico

convert output-16x16.ico output-32x32.ico output-48x48.ico output-64x64.ico \
    favicon.ico
