#!/usr/bin/python3
"""
Generates an /etc/shadow compatible hash. Primarily used for our puppet
configurations.
"""
import getpass
import bcrypt


# Uses the strongest available salt to generate a password.
# This will prompt for user input and print to the shell.
def main():
    """
    Actually does the work here.
    """
    password = bytes(getpass.getpass(), "UTF-8")
    pwhash = bcrypt.hashpw(password=password, salt=bcrypt.gensalt())
    print(f"Hash: '{pwhash}'")


if __name__ == "__main__":
    main()
