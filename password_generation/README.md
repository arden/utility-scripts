This simple script uses standard modules in python3 to generate a nice password hash. We primarily use this for puppet-controlled passwords since we don't actually want to plain text store them in the config ( or store the generation ).

```bash
./mkhash.py
```

Executing this command and providing a value will output something similar to the following:
[Generating a password hash](img/mkhash-output.png)


### External Links
* [How to generate a SHA-2 (sha256 or sha512) hashed password compatible with /etc/shadow](https://access.redhat.com/solutions/221403)
* [puppetlabs/accounts](https://forge.puppet.com/puppetlabs/accounts/readme)
