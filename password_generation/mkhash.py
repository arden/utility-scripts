#!/usr/bin/python3
"""
Generates an /etc/shadow compatible hash. Primarily used for our puppet
configurations.
"""
import crypt
import getpass


# Uses the strongest available salt to generate a password.
# This will prompt for user input and print to the shell.
def main():
    """
    Actually does the work here.
    """
    pwhash = crypt.crypt(getpass.getpass())
    print(f"Hash: '{pwhash}'")


if __name__ == "__main__":
    main()
